package com.ruyuan.little.project.rocketmq.api.order.dto;

import com.ruyuan.little.project.common.enums.MessageTypeEnum;

public class OrderMessageDTO {

    private String content;
    private MessageTypeEnum messageTypeEnum;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MessageTypeEnum getMessageTypeEnum() {
        return messageTypeEnum;
    }

    public void setMessageTypeEnum(MessageTypeEnum messageTypeEnum) {
        this.messageTypeEnum = messageTypeEnum;
    }

    @Override
    public String toString() {
        return "OrderMessageDTO{" +
                "content='" + content + '\'' +
                ", messageTypeEnum=" + messageTypeEnum +
                '}';
    }
}
