package com.ruyuan.little.project.rocketmq.admin.service;

import com.ruyuan.little.project.common.dto.CommonResponse;

/**
 * @author liangyi
 *
 */
public interface AdminOrderService {

    /**
     * 确认订单入住
     * @param orderNo 订单号
     * @param phoneNumber 手机号
     * @return 结果
     */
    CommonResponse confirmOrder(String orderNo,String phoneNumber);


    /**
     * 退房（完成订单）
     * @param orderNo 订单号
     * @param phoneNumber 手机号
     * @return 结果
     */
    CommonResponse finishOrder(String orderNo, String phoneNumber);
}
