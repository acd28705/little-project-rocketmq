package com.ruyuan.little.project.rocketmq.api.hotel.service;

import com.ruyuan.little.project.common.dto.CommonResponse;

public interface HotelRoomService {
    CommonResponse getRoomById(Long id, String phoneNumber);
}
