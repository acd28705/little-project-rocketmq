package com.ruyuan.little.project.rocketmq.admin.controller;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.admin.service.AdminOrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author liangyi
 */
@RestController
@RequestMapping(value = "/admin/order")
public class AdminOrderController {

    @Resource
    private AdminOrderService adminOrderService;

    /**
     * 入住订单
     * @param orderNo 订单号
     * @param phoneNumber 手机号
     * @return 结果
     */
    @GetMapping(value = "/confirmOrder")
    public CommonResponse confirmOrder(@RequestParam(value = "orderNo") String orderNo,
                                       @RequestParam(value = "phoneNumber") String phoneNumber){
        return adminOrderService.confirmOrder(orderNo,phoneNumber);
    }

    /**
     * 退房
     * @param orderNo 订单号
     * @param phoneNumber 手机号
     * @return 结果
     */
    @GetMapping(value = "/confirmOrder")
    public CommonResponse finishOrder(@RequestParam(value = "orderNo") String orderNo,
                                       @RequestParam(value = "phoneNumber") String phoneNumber){
        return adminOrderService.finishOrder(orderNo,phoneNumber);
    }


}
