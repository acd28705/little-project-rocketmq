package com.ruyuan.little.project.rocketmq.api.order.service;

import com.ruyuan.little.project.rocketmq.api.order.dto.OrderInfoDTO;

public interface OrderEventInformManager {

    /**
     * 通知创建订单事件
     *
     * @param orderInfoDTO 订单信息
     */
    void informCreateOrderEvent(OrderInfoDTO orderInfoDTO);

    /**
     * 通知订单取消事件
     * @param orderInfo
     */
    void informCancelOrderEvent(OrderInfoDTO orderInfo);

    /**
     * 通知订单支付事件
     * @param orderInfo
     */
    void informPayOrderEvent(OrderInfoDTO orderInfo);

    /**
     * 通知订单入住事件
     * @param orderInfo
     */
    void informConfirmOrderEvent(OrderInfoDTO orderInfo);

    /**
     * 通知退房事件
     * @param orderInfo
     */
    void informOrderFinishEvent(OrderInfoDTO orderInfo);
}
