package com.ruyuan.little.project.rocketmq.api.hotel.service.impl;


import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.ruyuan.little.project.rocketmq.api.hotel.dto.HotelRoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class HotelRoomCacheManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(HotelRoomCacheManager.class);

    /**
     * 房间内存缓存
     * 这里使用 map来做缓存容器，不好的地方在于，没有过期机制
     * 最好可以使用 guava cache
     */
    private ConcurrentHashMap<Long, HotelRoom> hotelRoomCacheMap = new ConcurrentHashMap<>();

    /**
     * getHotelRoomFromLocalCache
     * @param roomId
     * @return
     */
    public HotelRoom getHotelRoomFromLocalCache(Long roomId){
        HotelRoom hotelRoom = hotelRoomCacheMap.get(roomId);
        return hotelRoom;
    }

    public void updateLocalCache(Long roomId,HotelRoom hotelRoom){
        hotelRoomCacheMap.put(roomId,hotelRoom);
        LOGGER.info("hotel room local cache data:{}", JSON.toJSONString(hotelRoomCacheMap));
    }

}
