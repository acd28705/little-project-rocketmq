package com.ruyuan.little.project.rocketmq.api.order.consume;

import com.ruyuan.little.project.rocketmq.api.order.listener.OrderDelayMessageListener;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderDelayConsumerConfiguration {

    @Value("${rocketmq.namesrv.address}")
    private String namesrvAddress;

    @Value("${rocketmq.order.delay.topic}")
    private String orderDelayTopic;

    @Value("${rocketmq.order.delay.consumer.group}")
    private String orderDelayConsumerGroup;


    @Bean(value = "orderDelayConsumer")
    public DefaultMQPushConsumer orderDelayConsumer(OrderDelayMessageListener orderDelayMessageListener) throws MQClientException {
        DefaultMQPushConsumer pushConsumer = new DefaultMQPushConsumer(orderDelayConsumerGroup);
        pushConsumer.setNamesrvAddr(namesrvAddress);
        pushConsumer.subscribe(orderDelayTopic,"*");
        pushConsumer.setMessageListener(orderDelayMessageListener);
        pushConsumer.start();
        return pushConsumer;
    }

}
