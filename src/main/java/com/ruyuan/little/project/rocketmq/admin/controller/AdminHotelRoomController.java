package com.ruyuan.little.project.rocketmq.admin.controller;


import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.admin.dto.AdminHotelRoom;
import com.ruyuan.little.project.rocketmq.admin.service.AdminRoomService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/admin/hotel/room")
public class AdminHotelRoomController {


    @Resource
    private AdminRoomService adminRoomService;

    @PostMapping(value = "/update")
    public CommonResponse update(@RequestBody AdminHotelRoom adminHotelRoom) {
        return adminRoomService.update(adminHotelRoom);
    }




}
