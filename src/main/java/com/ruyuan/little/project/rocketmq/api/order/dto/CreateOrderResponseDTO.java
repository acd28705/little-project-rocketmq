package com.ruyuan.little.project.rocketmq.api.order.dto;

public class CreateOrderResponseDTO {

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 订单id
     */
    private Integer orderId;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

}
