package com.ruyuan.little.project.rocketmq.api.order.dto;

import java.math.BigDecimal;

public class OrderInfoDTO {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 店铺的id
     */
    private Integer beid;

    /**
     * 开放id
     */
    private String openId;

    /**
     * 房间id
     */
    private Integer roomId;

    /**
     * 酒店id
     */
    private Integer hotelId;

    /**
     * 酒店名字
     */
    private String hotelName;

    /**
     * 订购数量
     */
    private Integer total;

    /**
     * 订单总金额
     */
    private BigDecimal totalPrice;

    /**
     * 房主姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 预定时间
     */
    private String remark;

    /**
     * 入住开始时间
     */
    private String beginDate;

    /**
     * 入住结束时间
     */
    private String endDate;

    /**
     * 订单的状态
     */
    private Integer status;

    /**
     * 订单创建时间 Unix时间
     */
    private Integer createTime;

    /**
     * 订单支付时间 Unix时间
     */
    private Integer payTime;

    /**
     * 订单取消时间 Unix时间
     */
    private Integer cancelTime;

    /**
     * 订单商品信息
     */
    private OrderItemDTO orderItem;

    /**
     * 优惠券id
     */
    private Integer couponId;

    /**
     * 优惠券金额
     */
    private BigDecimal couponMoney;

    /**
     * 用户id
     */
    private Integer userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getBeid() {
        return beid;
    }

    public void setBeid(Integer beid) {
        this.beid = beid;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public Integer getPayTime() {
        return payTime;
    }

    public void setPayTime(Integer payTime) {
        this.payTime = payTime;
    }

    public Integer getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Integer cancelTime) {
        this.cancelTime = cancelTime;
    }

    public OrderItemDTO getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItemDTO orderItem) {
        this.orderItem = orderItem;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public BigDecimal getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(BigDecimal couponMoney) {
        this.couponMoney = couponMoney;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "OrderInfoDTO{" +
                "id=" + id +
                ", orderNo='" + orderNo + '\'' +
                ", beid=" + beid +
                ", openId='" + openId + '\'' +
                ", roomId=" + roomId +
                ", hotelId=" + hotelId +
                ", hotelName='" + hotelName + '\'' +
                ", total=" + total +
                ", totalPrice=" + totalPrice +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", remark='" + remark + '\'' +
                ", beginDate='" + beginDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", status=" + status +
                ", createTime=" + createTime +
                ", payTime=" + payTime +
                ", cancelTime=" + cancelTime +
                ", orderItem=" + orderItem +
                ", couponId=" + couponId +
                ", couponMoney=" + couponMoney +
                ", userId=" + userId +
                '}';
    }
}
