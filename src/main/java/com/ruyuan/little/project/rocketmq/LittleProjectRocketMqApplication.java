package com.ruyuan.little.project.rocketmq;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 小实战项目：使用rocketmq制作互联网酒店预订系统
 * @author 强军
 */
@SpringBootApplication
public class LittleProjectRocketMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(LittleProjectRocketMqApplication.class,args);
    }


}
