package com.ruyuan.little.project.rocketmq.api.hotel.service.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.common.enums.ErrorCodeEnum;
import com.ruyuan.little.project.common.enums.LittleProjectTypeEnum;
import com.ruyuan.little.project.mysql.api.MysqlApi;
import com.ruyuan.little.project.mysql.dto.MysqlRequestDTO;
import com.ruyuan.little.project.redis.api.RedisApi;
import com.ruyuan.little.project.rocketmq.api.hotel.dto.HotelRoom;
import com.ruyuan.little.project.rocketmq.api.hotel.dto.RoomDescription;
import com.ruyuan.little.project.rocketmq.api.hotel.dto.RoomPicture;
import com.ruyuan.little.project.rocketmq.api.hotel.enums.HotelBusinessErrorCodeEnum;
import com.ruyuan.little.project.rocketmq.api.hotel.service.HotelRoomService;
import com.ruyuan.little.project.rocketmq.common.constants.RedisKeyConstant;
import com.ruyuan.little.project.rocketmq.common.exception.BusinessException;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class HotelRoomServiceImpl implements HotelRoomService {

    private Logger LOGGER = LoggerFactory.getLogger(HotelRoomServiceImpl.class);

    /**
     * mysql dubbo接口api
     */
    @Reference(version = "1.0.0",
            interfaceClass = MysqlApi.class,
            cluster = "failfast",check = false)
    private MysqlApi mysqlApi;

    /**
     * redsi dubbo接口api
     */
    @Reference(version = "1.0.0",
            interfaceClass = RedisApi.class,
            cluster = "failfast",check = false)
    private RedisApi redisApi;

    @Resource
    private HotelRoomCacheManager hotelRoomCacheManager;

    /**
     * 查询房间详情
     *
     * 多级缓存 JVM  + Redis
     * @param roomId
     * @param phoneNumber
     * @return
     */
    @Override
    public CommonResponse getRoomById(Long roomId, String phoneNumber) {

        HotelRoom hotelRoom = hotelRoomCacheManager.getHotelRoomFromLocalCache(roomId);

        if (!Objects.isNull(hotelRoom)) {
            LOGGER.info("hotelId:{} data hit local cache", roomId);
            return CommonResponse.success(hotelRoom);
        }

        LOGGER.info("hotelId:{} data local cache miss try from redis query", roomId);

        LOGGER.info("start query room data from redis cache param:{}", roomId);
        CommonResponse<String> commonResponse = redisApi.get(RedisKeyConstant.HOTEL_ROOM_KEY_PREFIX + roomId, phoneNumber, LittleProjectTypeEnum.ROCKETMQ);
        LOGGER.info("end query room data from redis cache param:{}, commonResponse:{}", roomId, JSON.toJSONString(commonResponse));

        if (Objects.equals(commonResponse.getCode(), ErrorCodeEnum.SUCCESS.getCode())) {

            String data = commonResponse.getData();
            if (StringUtils.hasLength(data)) {
                LOGGER.info("hotelId:{} data hit redis cache ", roomId);
                return CommonResponse.success(JSON.parseObject(data, HotelRoom.class));
            }

        }

        LOGGER.info("hotelId:{} data local cache and redis cache miss try from db query", roomId);

        return CommonResponse.success(getHotelRoomById(roomId, phoneNumber));
    }

    /**
     * 根据房间id查询房间内容
     *
     * @param roomId          房间id
     * @param phoneNumber 手机号
     * @return 房间信息
     */
    private HotelRoom getHotelRoomById(Long roomId, String phoneNumber) {

        MysqlRequestDTO mysqlRequestDTO = new MysqlRequestDTO();
        mysqlRequestDTO.setSql("SELECT "
                + "id,"
                + "title, "
                + "pcate, "
                + "thumb_url, "
                + "description, "
                + "goods_banner, "
                + "marketprice, "
                + "productprice, "
                + "total,"
                + "createtime "
                + "FROM "
                + "t_shop_goods "
                + "WHERE "
                + "id = ?");
        mysqlRequestDTO.setPhoneNumber(phoneNumber);
        mysqlRequestDTO.setProjectTypeEnum(LittleProjectTypeEnum.ROCKETMQ);
        mysqlRequestDTO.setParams(Collections.singletonList(roomId));
        LOGGER.info("start query room detail param:{}", JSON.toJSONString(mysqlRequestDTO));
        CommonResponse<List<Map<String, Object>>> commonResponse = mysqlApi.query(mysqlRequestDTO);
        LOGGER.info("end query room detail param:{}, response:{}", JSON.toJSONString(mysqlRequestDTO), JSON.toJSONString(commonResponse));
        if (Objects.equals(commonResponse.getCode(), ErrorCodeEnum.SUCCESS.getCode())) {
            List<Map<String, Object>> mapList = commonResponse.getData();
            if (!CollectionUtils.isEmpty(mapList)) {
                List<HotelRoom> hotelRoomDetailList = mapList.stream().map(map -> {
                    HotelRoom detailDTO = new HotelRoom();
                    detailDTO.setId((Long) map.get("id"));
                    detailDTO.setTitle(String.valueOf(map.get("title")));
                    detailDTO.setPcate((Long) map.get("pcate"));
                    detailDTO.setThumbUrl(String.valueOf(map.get("thumb_url")));
                    detailDTO.setRoomDescription(JSON.parseObject(String.valueOf(map.get("description")), RoomDescription.class));
                    String goods_banner = String.valueOf(map.get("goods_banner"));
                    List<RoomPicture> roomPictures = JSON.parseArray(goods_banner, RoomPicture.class);
                    detailDTO.setGoods_banner(roomPictures);
                    detailDTO.setMarketprice((BigDecimal) map.get("marketprice"));
                    detailDTO.setProductprice((BigDecimal) map.get("productprice"));
                    detailDTO.setTotal((Integer) map.get("total"));
                    detailDTO.setCreatetime((Long) map.get("createtime"));
                    return detailDTO;
                }).collect(Collectors.toList());

                // 存入redis缓存
                HotelRoom hotelRoom = hotelRoomDetailList.get(0);

                hotelRoomCacheManager.updateLocalCache(roomId,hotelRoom);

                redisApi.set(RedisKeyConstant.HOTEL_ROOM_KEY_PREFIX + roomId,JSON.toJSONString(hotelRoom),phoneNumber,LittleProjectTypeEnum.ROCKETMQ);

                return hotelRoom;
            }
        }
        LOGGER.info("hotelId:{} data db not exist", roomId);
        // 房间不存在
        throw new BusinessException(HotelBusinessErrorCodeEnum.HOTEL_ROOM_NOT_EXIST.getMsg());
    }
}
