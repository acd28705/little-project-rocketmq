package com.ruyuan.little.project.rocketmq.api.coupon.consumer;

import com.ruyuan.little.project.rocketmq.api.coupon.listener.FirstLoginMessageListener;
import com.ruyuan.little.project.rocketmq.api.coupon.listener.OrderFinishedMessageListener;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 强军
 */
@Configuration
public class CouponConsumerConfiguration {


    /**
     * namesrv address
     */
    @Value("${rocketmq.namesrv.address}")
    private String namesrvAddress;

    /**
     * 登录topicgetApplications
     */
    @Value("${rocketmq.login.topic}")
    private String loginTopic;

    /**
     * 退房订单TOPIC
     */
    @Value("${rocketmq.order.finished.topic}")
    private String orderFinishTopic;

    /**
     * 登录消息consumerGroup
     */
    @Value("${rocketmq.login.consumer.group}")
    private String loginConsumerGroup;

    /**
     * 退房订单消费群组
     */
    @Value("${rocketmq.order.delay.consumer.group}")
    private String orderDelayConsumerGroup;

    /**
     * 登录消息消费者
     * @return
     * @throws MQClientException
     */
    @Bean(name = "loginConsumer")
    public DefaultMQPushConsumer loginConsumer(@Qualifier(value = "firstLoginMessageListener") FirstLoginMessageListener
                                                           firstLoginMessageListener) throws MQClientException {
        DefaultMQPushConsumer loginConsumer = new DefaultMQPushConsumer(loginConsumerGroup);
        loginConsumer.setNamesrvAddr(namesrvAddress);

        loginConsumer.subscribe(loginTopic,"*");
        loginConsumer.setMessageListener(firstLoginMessageListener);

        loginConsumer.start();
        return loginConsumer;
    }


    /**
     * 退房订单消费者
     *
     * @return orderFinishConsumer
     * @throws MQClientException
     */
    @Bean(name = "orderFinishedConsumer")
    public DefaultMQPushConsumer finishedConsumer(@Qualifier(value = "orderFinishedMessageListener") OrderFinishedMessageListener
                                                          orderFinishedMessageListener) throws MQClientException {
        DefaultMQPushConsumer orderFinishConsumer = new DefaultMQPushConsumer(orderDelayConsumerGroup);
        orderFinishConsumer.setNamesrvAddr(namesrvAddress);
        orderFinishConsumer.subscribe(orderFinishTopic, "*");
        orderFinishConsumer.setMessageListener(orderFinishedMessageListener);
        orderFinishConsumer.start();
        return orderFinishConsumer;
    }



}
