package com.ruyuan.little.project.rocketmq.admin.service.impl;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.admin.service.AdminOrderService;
import com.ruyuan.little.project.rocketmq.api.order.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author liangyi
 */
@Service
public class AdminOrderServiceImpl implements AdminOrderService {


    @Resource
    private OrderService orderService;

    @Override
    public CommonResponse confirmOrder(String orderNo, String phoneNumber) {
        orderService.informConfirmOrder(orderNo,phoneNumber);
        return CommonResponse.success();
    }

    /**
     * 退房：分布式事务改造
     * @param orderNo 订单号
     * @param phoneNumber 手机号
     * @return
     */
    @Override
    public CommonResponse finishOrder(String orderNo, String phoneNumber) {
        orderService.informFinishOrder(orderNo,phoneNumber);
        return CommonResponse.success();
    }
}
