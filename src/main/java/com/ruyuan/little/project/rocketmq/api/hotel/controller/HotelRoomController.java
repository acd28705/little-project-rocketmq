package com.ruyuan.little.project.rocketmq.api.hotel.controller;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.api.hotel.service.HotelRoomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/api/hotel")
public class HotelRoomController {

    @Resource
    private HotelRoomService hotelRoomService;

    private static final Logger LOGGER = LoggerFactory.getLogger(HotelRoomController.class);

    @GetMapping(value = "/getRoomById")
    public CommonResponse getRoomById(@RequestParam(value = "id") Long id,
                                      @RequestParam(value = "phoneNumber") String phoneNumber){
        return hotelRoomService.getRoomById(id,phoneNumber);
    }
}
