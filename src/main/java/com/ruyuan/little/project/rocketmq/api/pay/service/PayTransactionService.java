package com.ruyuan.little.project.rocketmq.api.pay.service;

import com.ruyuan.little.project.rocketmq.api.pay.dto.PayTransaction;

/**
 * @author 强军
 * 支付交易流水记录
 */
public interface PayTransactionService {
    /**
     * 保存支付流水记录
     * @param payTransaction 支付流水
     * @param phoneNumber 手机号
     * @return 记录流水结果
     */
    Boolean save(PayTransaction payTransaction,String phoneNumber);

}
