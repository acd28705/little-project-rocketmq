package com.ruyuan.little.project.rocketmq.api.order.service;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.api.order.dto.CreateOrderResponseDTO;
import com.ruyuan.little.project.rocketmq.api.order.dto.OrderInfoDTO;
import com.ruyuan.little.project.rocketmq.api.order.enums.OrderStatusEnum;

public interface OrderService {

    /**
     * 创建订单
     *
     * P.S. 通用响应对象包装的很好
     *
     * @param orderInfoDTO 订单信息
     * @return 结果
     */
    CommonResponse<CreateOrderResponseDTO> createOrder(OrderInfoDTO orderInfoDTO);

    /**
     * 取消订单
     * @param orderNo
     * @param phoneNumber
     * @return
     */
    CommonResponse cancelOrder(String orderNo,String phoneNumber);

    /**
     * 支付订单
     * @param orderNo
     * @param phoneNumber
     * @return
     */
    Integer informPayOrderSuccessed(String orderNo, String phoneNumber);

    /**
     * 入住
     * @param orderNo 订单号
     * @param phoneNumber 手机号码
     */
    void informConfirmOrder(String orderNo, String phoneNumber);

    /**
     * 退房（订单完成）
     * @param orderNo
     * @param phoneNumber
     */
    void informFinishOrder(String orderNo, String phoneNumber);

    /**
     * 更新订单状态
     * @param orderNo
     * @param orderStatusEnum
     * @param phoneNumber
     */
    void updateOrderStatus(String orderNo, OrderStatusEnum orderStatusEnum, String phoneNumber);

    /**
     * 查询订单状态
     * @param orderNo
     * @param phoneNumber
     * @return
     */
    Integer getOrderStatus(String orderNo, String phoneNumber);
}
