package com.ruyuan.little.project.rocketmq.api.pay.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 交易流水
 * @author 强军
 */
public class PayTransaction {

    private Long id;
    private String orderNo;
    private BigDecimal payableAmount;
    private String userPayAccount;
    private Integer transactionChannel;
    private String transactionNumber;
    private String finishPayTime;
    private String responseCode;
    private Integer status;
    private Date gmtCreate;
    private Date gmtModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getUserPayAccount() {
        return userPayAccount;
    }

    public void setUserPayAccount(String userPayAccount) {
        this.userPayAccount = userPayAccount;
    }

    public Integer getTransactionChannel() {
        return transactionChannel;
    }

    public void setTransactionChannel(Integer transactionChannel) {
        this.transactionChannel = transactionChannel;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getFinishPayTime() {
        return finishPayTime;
    }

    public void setFinishPayTime(String finishPayTime) {
        this.finishPayTime = finishPayTime;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}
