package com.ruyuan.little.project.rocketmq.api.login.producer;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoginProducerConfiguration {

    @Value("${rocketmq.login.producer.group}")
    private String loginProducerGroup;
    @Value("${rocketmq.namesrv.address}")
    private String namesrvAddress;

    /**
     * 登录消息生产者
     * @return MQClientException
     */
    @Bean(name = "loginProducer")
    public DefaultMQProducer loginProducer() throws MQClientException {
        DefaultMQProducer loginProducer = new DefaultMQProducer(loginProducerGroup);
        loginProducer.setNamesrvAddr(namesrvAddress);
        loginProducer.start();
        return loginProducer;
    }

}
