package com.ruyuan.little.project.rocketmq.api.order.controller;


import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.api.order.dto.CreateOrderResponseDTO;
import com.ruyuan.little.project.rocketmq.api.order.dto.OrderInfoDTO;
import com.ruyuan.little.project.rocketmq.api.order.service.OrderService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/api/order/")
public class OrderController {

    @Resource
    private OrderService orderService;

    /**
     * 预订房间 创建订单
     * @param orderInfoDTO 订单信息
     * @return 订单好
     */
    @PostMapping(value = "createOrder")
    public CommonResponse<CreateOrderResponseDTO> createOrder(OrderInfoDTO orderInfoDTO){
        return orderService.createOrder(orderInfoDTO);
    }

    /**
     * 取消订单
     * @param orderNo
     * @param phoneNumber
     * @return
     */
    @PostMapping(value = "cancelOrder")
    public CommonResponse cancelOrder(@RequestParam(value = "orderNo") String orderNo,
                                      @RequestParam(value = "phoneNumber") String phoneNumber){
        return orderService.cancelOrder(orderNo,phoneNumber);
    }

}
