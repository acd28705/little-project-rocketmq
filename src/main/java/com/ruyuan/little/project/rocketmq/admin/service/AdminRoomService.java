package com.ruyuan.little.project.rocketmq.admin.service;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.rocketmq.admin.dto.AdminHotelRoom;

public interface AdminRoomService {


    /**
     * 更新商品信息
     *
     * @param adminHotelRoom 请求体内容
     * @return
     */
    CommonResponse update(AdminHotelRoom adminHotelRoom);
}
