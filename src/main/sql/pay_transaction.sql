drop table if exists `pay_transaction`;
create table `pay_transaction`
(
    `id`                          bigint(20)    not null auto_increment primary key comment '主键',
    `transaction_number`          varchar(1024) not null comment '交易流水号，第三方平台生成',
    `order_no`                    varchar(1024) not null comment '订单号',
    `total_order_amount`          decimal(8, 2) not null comment '订单总金额',
    `discount_order_amount`       decimal(8, 2)          default null comment '订单优惠金额',
    `payable_order_amount`        decimal(8, 2) not null comment '订单应付金额',
    `transaction_channel`         tinyint(4)    not null comment '交易渠道 1-支付宝 2-微信',
    `transaction_channel_account` varchar(1024) not null default '222222' comment '交易渠道账户',
    `finish_pay_time`             varchar(1024) not null comment '第三方平台完成支付时间',
    `response_code`               varchar(1024) not null comment '第三方渠道返回的状态码',
    `user_pay_account`            varchar(1024) not null comment '用户支付账号',
    `status`                      varchar(1024) not null comment '支付状态',
    `phone_number`                varchar(1024) not null comment '手机号',
    `gmt_create`                  datetime      NOT NULL COMMENT '创建时间',
    `gmt_modified`                datetime      not null comment '更新时间'
) engine = Innodb
  auto_increment = 41
  default charset = utf8 comment ='支付交易流水';